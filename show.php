
	<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="../css/spectre.min.css">
		<link rel="stylesheet" href="../css/spectre-exp.min.css">
		<link rel="stylesheet" href="./css/spectre-icons.min.css">
	  </head>
	  <body>
		<header>
		  <h1> ADMIN AREA </h1>
		</header>

		<nav>
		  <ul>
			<li>
			  <a href="index.php">Home</a>
			</li>
			<li>
			  <a href="employees.php">Employees</a>
			</li>
		  </ul>
		</nav>

		<div class="container">
		  <div class="column">
			<div class="column col-10 col-mx-auto">
			  <?php

				if (isset($_GET["id"]) == FALSE) {
				  echo "<p>ID is missing </p>";
				}
				else {
				  // get the id from the URL
				  $id = $_GET["id"];
				  echo $id;
				  echo "<br />";

				  // @TODO: your database code should go somewhere here
				  //---------------------------------------------------
	$dbhost = "localhost";
	$dbuser = "root";
	$dbpass = "";
	$dbname = "cestar";

	// 1. Create a database connection
	$connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

	// show an error message if PHP cannot connect to the database
	if (mysqli_connect_errno())
	{
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  exit();
	}
	// 2. Perform database query
	$query = "SELECT * FROM employees ";
	$query .= "WHERE id = '". $id ."'";
	$results = mysqli_query($connection, $query);

	if ($results == FALSE) {
	  echo "Database query failed. <br/>";
	  echo "SQL command: " . $query;
	  exit();
	}

	//print_r($results);

      $person = mysqli_fetch_assoc($results);

				  // output the results to the screen
				  echo "<p><strong>First Name: </strong>" . $person["first_name"] . "</p>";
				  echo "<p><strong>Last Name: </strong>" . $person["last_name"] . "</p>";
				  echo "<p><strong>Hire Date: </strong>" . $person["hire_date"] . "</p>";

				  echo "<br/>";


				}
			  ?>

			  <p>
				<a href="employees.php" class="btn">Go Back</a>
			  </p>

			</div> <!--//col-10-->
		  </div> <!--//columns -->
		</div> <!--// container -->

		<footer>
		  &copy; <?php echo date("Y") ?> Cestar College
		</footer>
	  </body>
	</html>
