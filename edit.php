	<?php
if (isset($_GET["id"]) == FALSE) {
  // missing an id parameters, so
  // redirect person back to add employee page
  header("Location: " . "employees.php");
  exit();
}

$id = $_GET["id"];

// @TODO: Your code should show the person's information in the form

// @TODO: your database code should  here
//---------------------------------------------------
	$dbhost = "localhost";
	$dbuser = "root";
	$dbpass = "";
	$dbname = "cestar";

	$connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	if (mysqli_connect_errno())
	{
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  exit();
	}
	
	$sql 	 = "SELECT * FROM employees ";
	$sql 	.= "WHERE id='" . $id . "'";

	$results = mysqli_query($connection, $sql);
				
	if ($results == FALSE) {
		// there was an error in the sql 
		echo "Database query failed. <br/>";
		echo "SQL command: " . $query;
		exit();
	}
	
	$person = mysqli_fetch_assoc($results);

	print_r($person);
	
	// 5. Close database connection
	mysqli_close($connection);


//---------------------------------------------------




if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  // get items from DATABASE
  $person = [];
  $person["firstName"] = $_POST['firstName'];
  $person["lastName"] = $_POST['lastName'];
  $person["hireDate"] = $_POST['hireDate'];

  // @TODO: your database code should  here
  //---------------------------------------------------

	$dbhost = "localhost";
	$dbuser = "root";
	$dbpass = "";
	$dbname = "cestar";

	$connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	if (mysqli_connect_errno())
	{
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  exit();
	}
	
	$sql 	 = "UPDATE EMPLOYEES SET ";
	$sql 	.= "first_name='" . $person["firstName"] . "', ";
	$sql 	.= "last_name='" . $person["lastName"] . "', ";
	$sql 	.= "hire_date='" . $person["hireDate"] . "' ";
	$sql 	.= "WHERE id= '" . $id . "' ";
	$sql    .= "LIMIT 1";
		
	echo $sql;
	
	

	$results = mysqli_query($connection, $sql);
				
	if ($results == FALSE) {
		// there was an error in the sql 
		echo "Database query failed. <br/>";
		echo "SQL command: " . $query;
		exit();
	}
	

	// 5. Close database connection
	mysqli_close($connection);

  //---------------------------------------------------

  // @TODO: delete these two statement after your add your db code
  echo "I got a POST request! <br />";
  print_r($_POST);

}








?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="../css/spectre.min.css">
    <link rel="stylesheet" href="../css/spectre-exp.min.css">
    <link rel="stylesheet" href="./css/spectre-icons.min.css">
  </head>
  <body>
    <header>
      <h1> ADMIN AREA </h1>
    </header>

    <nav>
      <ul>
        <li>
          <a href="index.php">Home</a>
        </li>
        <li>
          <a href="employees.php">Employees</a>
        </li>
      </ul>
    </nav>

    <div class="container">
      <div class = "columns">
        <div class="column col-10 col-mx-auto">

          <!-- @TODO: fill in the ACTION and METHOD portions -->
          <form action="<?php echo "edit.php?id=" . $id; ?>" method="POST" class="form-group">

            <!-- @TODO: fill in the value="" parameters -->
            <label class="form-label" for="firstName">First Name</label>
            <input type="text" name="firstName" value="<?php echo $person['first_name']; ?>" />

            <label class="form-label" for="lastName">Last Name</label>
            <input type="text" name="lastName" value="<?php echo $person['last_name']; ?>" />

            <label class="form-label" for="hireDate">Hire Date</label>
            <input type="text" name="hireDate" value="<?php echo $person['hire_date']; ?>" />

            <p>
              <input type="submit" value="Update Employee" />
            </p>
          </form>

          <p>
            <a href="employees.php" class="btn">Go Back</a>
          </p>

        </div> <!--// col-12 -->
      </div> <!-- // column -->
    </div> <!--// container -->

    <footer>
      &copy; <?php echo date("Y") ?> Cestar College
    </footer>

  </body>
</html>
