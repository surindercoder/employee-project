<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  // run this code if you get a POST
  echo "<h2> I got a POST request! </h2>";
}
else if ($_SERVER["REQUEST_METHOD"] == "GET") {
  // run this code if you get a GET request!
  echo "<h2> I got a GET REQUEST! </h2>";
}
?>
<!-- This HTML will always run. Doesn't matter if you have a GET or POST requst -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>
    <div class="container">
      <div class = "columns">
        <div class="column col-10 col-mx-auto">
          <!-- @TOOD: fill in the ACTION and METHOD -->
          <form action="page55.php" method="POST" class="form-group">

            <label class="form-label" for="firstName">First Name</label>
            <input type="text" name="firstName" value="" />

            <p>
              <input type="submit" value="GO GO GO!" />
            </p>
          </form>


        </div> <!--// col-12 -->
      </div> <!-- // column -->
    </div> <!--// container -->

  </body>
</html>
