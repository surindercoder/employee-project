<?php

// This guide demonstrates the five fundamental steps
// of database interaction using PHP.

// Credentials
$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "cestar";

// 1. Create a database connection
$connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

// show an error message if PHP cannot connect to the database
if (mysqli_connect_errno())
{
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
}
// 2. Perform database query
$query = "SELECT * FROM employees";
$results = mysqli_query($connection, $query);

if ($results == FALSE) {
  echo "Database query failed. <br/>";
  echo "SQL command: " . $query;
  exit();
}

//print_r($results);

// 3. Use returned data (if any)

// get the total number of rows from the database
$totalRows = mysqli_num_rows($results);
//echo $totalRows;

/*
for ($i = 0; $i < $totalRows; $i++) {
  $employee = mysqli_fetch_assoc($results);
  print_r($employee);
  echo "<br />";
}
*/

// another way
while ($employee = mysqli_fetch_assoc($results)) {
  print_r($employee);
  echo "<br />";
}


// 4. Release returned data
mysqli_free_result($results);

// 5. Close database connection
mysqli_close($connection);

?>
