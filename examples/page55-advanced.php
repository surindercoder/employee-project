<?php

// error handling in case user didn't include an ID parameter
// in the URL
if (isset($_GET["id"]) == FALSE) {
  echo "<p style='color:red'>You didn't include an <strong>id</strong> parameter in the url! </p>";
  //exit();
}

// get and save the ID parameter to a variable
$id = $_GET["id"];

// This code will always run. Doesn't matter if you have a GET or POST request
echo "<p style='color:blue'>The <strong>id</strong> is: " . $id . "</p>";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  // code you want to run when you have a GET request
  echo "<h3> I got a GET request! </h3>";
}
elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
  // code you want to run when you have a POST request
  echo "<h3> I got a POST request! </h3>";
}
?>

<!-- This HTML will always run. Doesn't matter if you have a GET or POST requst -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
  </head>
  <body>
    <div class="container">
      <div class = "columns">
        <div class="column col-10 col-mx-auto">
          <!-- @TOOD: fill in the ACTION and METHOD -->
          <form action="page55-advanced.php" method="POST" class="form-group">

            <label class="form-label" for="firstName">First Name</label>
            <input type="text" name="firstName" value="" />

            <p>
              <input type="submit" value="GO GO GO!" />
            </p>
          </form>


        </div> <!--// col-12 -->
      </div> <!-- // column -->
    </div> <!--// container -->

  </body>
</html>
